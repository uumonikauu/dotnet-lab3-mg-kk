﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JTTT_MG_KK
{
    class PrzetwarzanieZadan
    {

        public string GetPageHtml(string url)
        {
            using (WebClient wc = new WebClient())
            {
                // Pobieramy zawartość strony spod adresu url jako ciąg bajtów
                byte[] data = wc.DownloadData(url);
                // Przekształcamy ciąg bajtów na string przy użyciu kodowania UTF-8. To oczywiście powinno zależeć od właściwego kodowania
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));

                // Wersja uproszczona bez uwzględniania kodowania:
                // string html = wc.DownloadString(url);

                return html;
            }
        }

        public void ObslugaKlikniecia(string url, string tekst, string email)
        {
            HtmlDocument doc = new HtmlDocument();
            string pageHtml = GetPageHtml(url);
            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                







                /*
                Console.WriteLine("---------");

                // Wyświetlamy nazwę node'a (powinno byc img")
                Console.WriteLine("Node name: " + node.Name);

                // Każdy node ma zestaw atrybutów - nas interesują atrybuty src oraz alt

                // Wyświetlamy wartość atrybuty src
                Console.WriteLine("Src value: " + node.GetAttributeValue("src", ""));
                // Wyświetlamy wartość atrybuty alt
                Console.WriteLine("Alt value: " + node.GetAttributeValue("alt", ""));

                // Oczywiscie w aplikacji JTTT nie będziemy tego wyświetlać tylko będziemy analizować 
                // wartość atrybutów node'a jako string

                // Wszystkie powyższe operacje można napisać zdecydowanie prościej i składniej na przyklad za pomoca wyrazenia LINQ
                // Ten zapis jest tylko do celów ćwiczebnych 
                 */
            }

        }

    }
}
